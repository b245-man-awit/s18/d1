// console.log("Hello world");

// let nickName = "shamee"

function printInput(){
	let nickName = prompt("Enter your nickname");
	console.log("Hi, " + nickName);
}

// printInput();

// Consider this function
	// parameter passing

	function printName(name){
		console.log("My name is " + name)
	}

	//argument
	printName("Shamee");
	printName("Sharmaineee");
	printName()

// [SECTION] Parameters and Arguments
	
	// Parameter
		// "name" is called a parameter
		// acts as a named variable/container that only exists inside a function.
		// it is use to store info that is provided to a function

	// Arguments
		// "Shamee" is the value/data passed directly into the function.
		// Values passed when invoking a function are called arguments.
		// Those arguments are then stored as the parameters within the function.

	function checkDivisibilityBy8(num){
			let remainder = num % 8;
			console.log("The remainder of " + num + " divided by 8 is: " + remainder);
			let isDivisibleBy8 = remainder === 0;
			console.log("Is " + num + " divisible by 8?");
			console.log(isDivisibleBy8);
		}

		checkDivisibilityBy8(40);

		let num1 = 64;
		checkDivisibilityBy8(num1);

		// A variable as the argument.
		/*let num1 = 64;
		checkDivisibilityBy8(num1);*/

// [SECTION] Function as Arguments

		// Function Parameters can also accepts other function as arguments.
		// Some complex functions use other functions as arguments to perform more complicated results.
		// This be further seen in array methods

		function argumentFunc(){
			console.log("This function was passed as an argument before the message was printed.")
		}

		function invokeFunc(argFunc){
			// console.log(argFunc);
			argFunc();
		}
		// Adding and removing the "()" impacts the output of javascript.
			// function is used with parenthesis it denotes invoke/call.
			// function is used without a parenthesis is normally associated with using a function as an argument to another function.

		invokeFunc(argumentFunc);

// [SECTION] Multiple Parameters

		// Multiple "arguments" will correspond to the number of "parameters" declared in a function in succeeding order.

		function createFullName(firstName, middleName, lastName){
			console.log(firstName+" "+middleName+" "+lastName);
		}

		createFullName("Juan", "Enye","Dela Cruz");
		createFullName("Juan", "Dela Cruz"); //magiging undefined apelido
		createFullName("Juan", "Enye", "Dela Cruz", "Junior"); //added arguments will not show in the output

		//Using variables as arguments
		let firstName = "John";
		let middleName = "Doe";
		let lastName = "Smith";

		createFullName (firstName, middleName, lastName);

		// Note the order of arguments is same with the order of the parameters. the first argument will be stored in the first parameter, second argument will be stored in the second parameter and so on.


// [SECTION] Return Statement
	// allows us to output a value from a function to be passed to the line/code block of the code that invoked/called the function.		
	// we can further use/manipulate in our program instead of only printing/displaying it on the console.	
	
	function returnFullName(firstName, middleName, lastName){
		// expected return Jeffrey Smith Doe
		return firstName + " " + middleName + " " + lastName;
		// return indicates the end of function execution
		//it will ignore any codes after return statement.
		console.log(firstName + " " + middleName + " " + lastName);
	}

	let completeName = returnFullName("Jeffrey", "Smith", "Doe");
	console.log(completeName);

	// You can also create a variable inside the function to contain the result and return the variable.

	function returnAddress(city, country){
		let fullAddress = city + ", " +country;
		return fullAddress
	}

	let myAddress = returnAddress("Cebu CIty", "Cebu");
	console.log(myAddress);
	console.log(returnAddress("Cebu CIty", "Cebu"));

	// when a dunction only has a console.log() to display the result it will return undefined instead.

	function printPlayerInfo(userName, level, job){
		console.log("Username: "+userName);
		console.log("Level: "+level);
		console.log("Job: "+job);

	}

	let user = printPlayerInfo("knight_white", 95, "Paladin");
	// You cannot save any value from printPlayerInfo() because it does not return anything
	console.log(user);